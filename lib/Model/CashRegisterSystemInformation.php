<?php
/**
 * CashRegisterSystemInformation
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.12
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * CashRegisterSystemInformation Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class CashRegisterSystemInformation implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'CashRegisterSystemInformation';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'manufacturer' => 'string',
        'manufacturing_model' => 'string',
        'manufacturing_number' => 'string',
        'manufacturing_version' => 'string',
        'ccu_manufacturing_number' => 'string',
        'cash_register_model' => 'string',
        'support_enabled' => 'bool'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'manufacturer' => null,
        'manufacturing_model' => null,
        'manufacturing_number' => null,
        'manufacturing_version' => null,
        'ccu_manufacturing_number' => null,
        'cash_register_model' => null,
        'support_enabled' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'manufacturer' => 'manufacturer',
        'manufacturing_model' => 'manufacturingModel',
        'manufacturing_number' => 'manufacturingNumber',
        'manufacturing_version' => 'manufacturingVersion',
        'ccu_manufacturing_number' => 'ccuManufacturingNumber',
        'cash_register_model' => 'cashRegisterModel',
        'support_enabled' => 'supportEnabled'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'manufacturer' => 'setManufacturer',
        'manufacturing_model' => 'setManufacturingModel',
        'manufacturing_number' => 'setManufacturingNumber',
        'manufacturing_version' => 'setManufacturingVersion',
        'ccu_manufacturing_number' => 'setCcuManufacturingNumber',
        'cash_register_model' => 'setCashRegisterModel',
        'support_enabled' => 'setSupportEnabled'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'manufacturer' => 'getManufacturer',
        'manufacturing_model' => 'getManufacturingModel',
        'manufacturing_number' => 'getManufacturingNumber',
        'manufacturing_version' => 'getManufacturingVersion',
        'ccu_manufacturing_number' => 'getCcuManufacturingNumber',
        'cash_register_model' => 'getCashRegisterModel',
        'support_enabled' => 'getSupportEnabled'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['manufacturer'] = isset($data['manufacturer']) ? $data['manufacturer'] : null;
        $this->container['manufacturing_model'] = isset($data['manufacturing_model']) ? $data['manufacturing_model'] : null;
        $this->container['manufacturing_number'] = isset($data['manufacturing_number']) ? $data['manufacturing_number'] : null;
        $this->container['manufacturing_version'] = isset($data['manufacturing_version']) ? $data['manufacturing_version'] : null;
        $this->container['ccu_manufacturing_number'] = isset($data['ccu_manufacturing_number']) ? $data['ccu_manufacturing_number'] : null;
        $this->container['cash_register_model'] = isset($data['cash_register_model']) ? $data['cash_register_model'] : null;
        $this->container['support_enabled'] = isset($data['support_enabled']) ? $data['support_enabled'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets manufacturer
     *
     * @return string
     */
    public function getManufacturer()
    {
        return $this->container['manufacturer'];
    }

    /**
     * Sets manufacturer
     *
     * @param string $manufacturer manufacturer
     *
     * @return $this
     */
    public function setManufacturer($manufacturer)
    {
        $this->container['manufacturer'] = $manufacturer;

        return $this;
    }

    /**
     * Gets manufacturing_model
     *
     * @return string
     */
    public function getManufacturingModel()
    {
        return $this->container['manufacturing_model'];
    }

    /**
     * Sets manufacturing_model
     *
     * @param string $manufacturing_model manufacturing_model
     *
     * @return $this
     */
    public function setManufacturingModel($manufacturing_model)
    {
        $this->container['manufacturing_model'] = $manufacturing_model;

        return $this;
    }

    /**
     * Gets manufacturing_number
     *
     * @return string
     */
    public function getManufacturingNumber()
    {
        return $this->container['manufacturing_number'];
    }

    /**
     * Sets manufacturing_number
     *
     * @param string $manufacturing_number manufacturing_number
     *
     * @return $this
     */
    public function setManufacturingNumber($manufacturing_number)
    {
        $this->container['manufacturing_number'] = $manufacturing_number;

        return $this;
    }

    /**
     * Gets manufacturing_version
     *
     * @return string
     */
    public function getManufacturingVersion()
    {
        return $this->container['manufacturing_version'];
    }

    /**
     * Sets manufacturing_version
     *
     * @param string $manufacturing_version manufacturing_version
     *
     * @return $this
     */
    public function setManufacturingVersion($manufacturing_version)
    {
        $this->container['manufacturing_version'] = $manufacturing_version;

        return $this;
    }

    /**
     * Gets ccu_manufacturing_number
     *
     * @return string
     */
    public function getCcuManufacturingNumber()
    {
        return $this->container['ccu_manufacturing_number'];
    }

    /**
     * Sets ccu_manufacturing_number
     *
     * @param string $ccu_manufacturing_number ccu_manufacturing_number
     *
     * @return $this
     */
    public function setCcuManufacturingNumber($ccu_manufacturing_number)
    {
        $this->container['ccu_manufacturing_number'] = $ccu_manufacturing_number;

        return $this;
    }

    /**
     * Gets cash_register_model
     *
     * @return string
     */
    public function getCashRegisterModel()
    {
        return $this->container['cash_register_model'];
    }

    /**
     * Sets cash_register_model
     *
     * @param string $cash_register_model cash_register_model
     *
     * @return $this
     */
    public function setCashRegisterModel($cash_register_model)
    {
        $this->container['cash_register_model'] = $cash_register_model;

        return $this;
    }

    /**
     * Gets support_enabled
     *
     * @return bool
     */
    public function getSupportEnabled()
    {
        return $this->container['support_enabled'];
    }

    /**
     * Sets support_enabled
     *
     * @param bool $support_enabled support_enabled
     *
     * @return $this
     */
    public function setSupportEnabled($support_enabled)
    {
        $this->container['support_enabled'] = $support_enabled;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


