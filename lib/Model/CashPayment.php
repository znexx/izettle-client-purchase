<?php
/**
 * CashPayment
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.12
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * CashPayment Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class CashPayment implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'CashPayment';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'cash_payment_uuid' => 'string',
        'amount' => 'int',
        'handed_amount' => 'int',
        'gratuity_amount' => 'int',
        'cash_payment_uuid1' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'cash_payment_uuid' => 'uuid',
        'amount' => 'int64',
        'handed_amount' => 'int64',
        'gratuity_amount' => 'int64',
        'cash_payment_uuid1' => 'uuid'
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'cash_payment_uuid' => 'cashPaymentUUID',
        'amount' => 'amount',
        'handed_amount' => 'handedAmount',
        'gratuity_amount' => 'gratuityAmount',
        'cash_payment_uuid1' => 'cashPaymentUUID1'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'cash_payment_uuid' => 'setCashPaymentUuid',
        'amount' => 'setAmount',
        'handed_amount' => 'setHandedAmount',
        'gratuity_amount' => 'setGratuityAmount',
        'cash_payment_uuid1' => 'setCashPaymentUuid1'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'cash_payment_uuid' => 'getCashPaymentUuid',
        'amount' => 'getAmount',
        'handed_amount' => 'getHandedAmount',
        'gratuity_amount' => 'getGratuityAmount',
        'cash_payment_uuid1' => 'getCashPaymentUuid1'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['cash_payment_uuid'] = isset($data['cash_payment_uuid']) ? $data['cash_payment_uuid'] : null;
        $this->container['amount'] = isset($data['amount']) ? $data['amount'] : null;
        $this->container['handed_amount'] = isset($data['handed_amount']) ? $data['handed_amount'] : null;
        $this->container['gratuity_amount'] = isset($data['gratuity_amount']) ? $data['gratuity_amount'] : null;
        $this->container['cash_payment_uuid1'] = isset($data['cash_payment_uuid1']) ? $data['cash_payment_uuid1'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['amount'] === null) {
            $invalidProperties[] = "'amount' can't be null";
        }
        if ($this->container['handed_amount'] === null) {
            $invalidProperties[] = "'handed_amount' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets cash_payment_uuid
     *
     * @return string
     */
    public function getCashPaymentUuid()
    {
        return $this->container['cash_payment_uuid'];
    }

    /**
     * Sets cash_payment_uuid
     *
     * @param string $cash_payment_uuid cash_payment_uuid
     *
     * @return $this
     */
    public function setCashPaymentUuid($cash_payment_uuid)
    {
        $this->container['cash_payment_uuid'] = $cash_payment_uuid;

        return $this;
    }

    /**
     * Gets amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->container['amount'];
    }

    /**
     * Sets amount
     *
     * @param int $amount amount
     *
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->container['amount'] = $amount;

        return $this;
    }

    /**
     * Gets handed_amount
     *
     * @return int
     */
    public function getHandedAmount()
    {
        return $this->container['handed_amount'];
    }

    /**
     * Sets handed_amount
     *
     * @param int $handed_amount handed_amount
     *
     * @return $this
     */
    public function setHandedAmount($handed_amount)
    {
        $this->container['handed_amount'] = $handed_amount;

        return $this;
    }

    /**
     * Gets gratuity_amount
     *
     * @return int
     */
    public function getGratuityAmount()
    {
        return $this->container['gratuity_amount'];
    }

    /**
     * Sets gratuity_amount
     *
     * @param int $gratuity_amount gratuity_amount
     *
     * @return $this
     */
    public function setGratuityAmount($gratuity_amount)
    {
        $this->container['gratuity_amount'] = $gratuity_amount;

        return $this;
    }

    /**
     * Gets cash_payment_uuid1
     *
     * @return string
     */
    public function getCashPaymentUuid1()
    {
        return $this->container['cash_payment_uuid1'];
    }

    /**
     * Sets cash_payment_uuid1
     *
     * @param string $cash_payment_uuid1 cash_payment_uuid1
     *
     * @return $this
     */
    public function setCashPaymentUuid1($cash_payment_uuid1)
    {
        $this->container['cash_payment_uuid1'] = $cash_payment_uuid1;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


