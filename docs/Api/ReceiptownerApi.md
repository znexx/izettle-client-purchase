# Swagger\Client\ReceiptownerApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clearReceiptOwnerContactInformation**](ReceiptownerApi.md#clearReceiptOwnerContactInformation) | **DELETE** /receiptowner/{purchaseUuid} | 


# **clearReceiptOwnerContactInformation**
> clearReceiptOwnerContactInformation($purchase_uuid)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ReceiptownerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$purchase_uuid = "purchase_uuid_example"; // string | 

try {
    $apiInstance->clearReceiptOwnerContactInformation($purchase_uuid);
} catch (Exception $e) {
    echo 'Exception when calling ReceiptownerApi->clearReceiptOwnerContactInformation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchase_uuid** | [**string**](../Model/.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

