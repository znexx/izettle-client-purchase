# Swagger\Client\CashregisterApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**close**](CashregisterApi.md#close) | **POST** /cashregister/{cashRegisterUUID}/close | Closes the cash register and generates a Z-report
[**createCashRegisterPurchaseRefund**](CashregisterApi.md#createCashRegisterPurchaseRefund) | **POST** /cashregister/{cashRegisterUUID}/purchase/refund | Create refund purchase with a specific cash register. After we save a receipt, it will contact the ccu to get a control code.
[**createShoppingCartForCashRegister**](CashregisterApi.md#createShoppingCartForCashRegister) | **POST** /cashregister/{cashRegisterUUID}/shopping-cart | Create shopping cart
[**createShoppingCartForCashRegister1**](CashregisterApi.md#createShoppingCartForCashRegister1) | **POST** /cashregister/{cashRegisterUUID}/shopping-cart/refund | Create refund shopping cart
[**get**](CashregisterApi.md#get) | **GET** /cashregister | Returns the users cash register, if is any and opened
[**getAllActive**](CashregisterApi.md#getAllActive) | **GET** /cashregister/all | Lists a merchant&#39;s cash registers if user is owner
[**getAvailable**](CashregisterApi.md#getAvailable) | **GET** /cashregister/available | Lists all available merchant&#39;s cash registers
[**getZReport**](CashregisterApi.md#getZReport) | **GET** /cashregister/{cashRegisterUUID}/zreport/{zReportNumber} | Get a specific Z-report
[**getZReports**](CashregisterApi.md#getZReports) | **GET** /cashregister/zreports | Get z-report history
[**isCashRegisterMerchant**](CashregisterApi.md#isCashRegisterMerchant) | **GET** /cashregister/status | Tells if a user&#39;s organization is a cash register merchant
[**mailJournal**](CashregisterApi.md#mailJournal) | **POST** /cashregister/{cashRegisterUUID}/journal/send-by-email | Send electronic journal by emailSends the electronic journal by email to the email address specified by the organization&#39;s &#39;contact_email&#39;
[**open**](CashregisterApi.md#open) | **POST** /cashregister/open | Opens a cash register for the calling client. The cash register opened will be one of the currently available (closed) cash registers for the clients organization
[**openSpecificCashRegister**](CashregisterApi.md#openSpecificCashRegister) | **POST** /cashregister/{cashRegisterUUID}/open | Opens a specific cash register for the calling client
[**sendXReportEmail**](CashregisterApi.md#sendXReportEmail) | **POST** /cashregister/{cashRegisterUUID}/xreport/send | Send a X-report for the cash register to the email address
[**sendZReportEmail**](CashregisterApi.md#sendZReportEmail) | **POST** /cashregister/{cashRegisterUUID}/zreport/{zReportNumber}/send | Send a specific Z-report by email
[**xReport**](CashregisterApi.md#xReport) | **GET** /cashregister/{cashRegisterUUID}/xreport | Generates a X-report for the cash register


# **close**
> \Swagger\Client\Model\ZReportResponse close($cash_register_uuid)

Closes the cash register and generates a Z-report



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cash_register_uuid = "cash_register_uuid_example"; // string | 

try {
    $result = $apiInstance->close($cash_register_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->close: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_register_uuid** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\ZReportResponse**](../Model/ZReportResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createCashRegisterPurchaseRefund**
> \Swagger\Client\Model\PurchaseReceiptResponse createCashRegisterPurchaseRefund($cash_register_uuid, $body)

Create refund purchase with a specific cash register. After we save a receipt, it will contact the ccu to get a control code.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cash_register_uuid = "cash_register_uuid_example"; // string | 
$body = new \Swagger\Client\Model\CreateRefundPurchaseRequest(); // \Swagger\Client\Model\CreateRefundPurchaseRequest | 

try {
    $result = $apiInstance->createCashRegisterPurchaseRefund($cash_register_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->createCashRegisterPurchaseRefund: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_register_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\CreateRefundPurchaseRequest**](../Model/CreateRefundPurchaseRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PurchaseReceiptResponse**](../Model/PurchaseReceiptResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createShoppingCartForCashRegister**
> \Swagger\Client\Model\CreateShoppingCartResponse createShoppingCartForCashRegister($cash_register_uuid, $body)

Create shopping cart



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cash_register_uuid = "cash_register_uuid_example"; // string | 
$body = new \Swagger\Client\Model\CreateShoppingCartRequest(); // \Swagger\Client\Model\CreateShoppingCartRequest | 

try {
    $result = $apiInstance->createShoppingCartForCashRegister($cash_register_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->createShoppingCartForCashRegister: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_register_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\CreateShoppingCartRequest**](../Model/CreateShoppingCartRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CreateShoppingCartResponse**](../Model/CreateShoppingCartResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createShoppingCartForCashRegister1**
> \Swagger\Client\Model\CreateShoppingCartResponse createShoppingCartForCashRegister1($cash_register_uuid, $body)

Create refund shopping cart



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cash_register_uuid = "cash_register_uuid_example"; // string | 
$body = new \Swagger\Client\Model\CreateRefundShoppingCartRequest(); // \Swagger\Client\Model\CreateRefundShoppingCartRequest | 

try {
    $result = $apiInstance->createShoppingCartForCashRegister1($cash_register_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->createShoppingCartForCashRegister1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_register_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\CreateRefundShoppingCartRequest**](../Model/CreateRefundShoppingCartRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CreateShoppingCartResponse**](../Model/CreateShoppingCartResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **get**
> \Swagger\Client\Model\CashRegisterResponseWrapper get()

Returns the users cash register, if is any and opened



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\CashRegisterResponseWrapper**](../Model/CashRegisterResponseWrapper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllActive**
> \Swagger\Client\Model\CashRegistersResponse getAllActive($include_inactive)

Lists a merchant's cash registers if user is owner



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$include_inactive = true; // bool | 

try {
    $result = $apiInstance->getAllActive($include_inactive);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->getAllActive: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **include_inactive** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\CashRegistersResponse**](../Model/CashRegistersResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAvailable**
> \Swagger\Client\Model\CashRegistersResponse getAvailable()

Lists all available merchant's cash registers



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getAvailable();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->getAvailable: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\CashRegistersResponse**](../Model/CashRegistersResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getZReport**
> \Swagger\Client\Model\ZReportResponse getZReport($cash_register_uuid, $z_report_number)

Get a specific Z-report



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cash_register_uuid = "cash_register_uuid_example"; // string | 
$z_report_number = 789; // int | 

try {
    $result = $apiInstance->getZReport($cash_register_uuid, $z_report_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->getZReport: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_register_uuid** | [**string**](../Model/.md)|  |
 **z_report_number** | **int**|  |

### Return type

[**\Swagger\Client\Model\ZReportResponse**](../Model/ZReportResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getZReports**
> \Swagger\Client\Model\ZReportHistoryResponse getZReports($last_report_hash, $limit, $descending)

Get z-report history



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$last_report_hash = 789; // int | 
$limit = 56; // int | 
$descending = true; // bool | 

try {
    $result = $apiInstance->getZReports($last_report_hash, $limit, $descending);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->getZReports: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **last_report_hash** | **int**|  | [optional]
 **limit** | **int**|  | [optional]
 **descending** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\ZReportHistoryResponse**](../Model/ZReportHistoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **isCashRegisterMerchant**
> bool isCashRegisterMerchant()

Tells if a user's organization is a cash register merchant



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->isCashRegisterMerchant();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->isCashRegisterMerchant: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **mailJournal**
> mailJournal($cash_register_uuid)

Send electronic journal by emailSends the electronic journal by email to the email address specified by the organization's 'contact_email'



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cash_register_uuid = "cash_register_uuid_example"; // string | 

try {
    $apiInstance->mailJournal($cash_register_uuid);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->mailJournal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_register_uuid** | [**string**](../Model/.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **open**
> \Swagger\Client\Model\CashRegisterResponseWrapper open($body)

Opens a cash register for the calling client. The cash register opened will be one of the currently available (closed) cash registers for the clients organization



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\OpenCashRegisterRequest(); // \Swagger\Client\Model\OpenCashRegisterRequest | 

try {
    $result = $apiInstance->open($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->open: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\OpenCashRegisterRequest**](../Model/OpenCashRegisterRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CashRegisterResponseWrapper**](../Model/CashRegisterResponseWrapper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **openSpecificCashRegister**
> \Swagger\Client\Model\CashRegisterResponseWrapper openSpecificCashRegister($cash_register_uuid, $body)

Opens a specific cash register for the calling client



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cash_register_uuid = "cash_register_uuid_example"; // string | 
$body = new \Swagger\Client\Model\OpenCashRegisterRequest(); // \Swagger\Client\Model\OpenCashRegisterRequest | 

try {
    $result = $apiInstance->openSpecificCashRegister($cash_register_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->openSpecificCashRegister: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_register_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\OpenCashRegisterRequest**](../Model/OpenCashRegisterRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CashRegisterResponseWrapper**](../Model/CashRegisterResponseWrapper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sendXReportEmail**
> sendXReportEmail($cash_register_uuid, $lang)

Send a X-report for the cash register to the email address



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cash_register_uuid = "cash_register_uuid_example"; // string | 
$lang = "lang_example"; // string | 

try {
    $apiInstance->sendXReportEmail($cash_register_uuid, $lang);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->sendXReportEmail: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_register_uuid** | [**string**](../Model/.md)|  |
 **lang** | **string**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sendZReportEmail**
> sendZReportEmail($cash_register_uuid, $z_report_number, $lang)

Send a specific Z-report by email



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cash_register_uuid = "cash_register_uuid_example"; // string | 
$z_report_number = 789; // int | 
$lang = "lang_example"; // string | 

try {
    $apiInstance->sendZReportEmail($cash_register_uuid, $z_report_number, $lang);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->sendZReportEmail: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_register_uuid** | [**string**](../Model/.md)|  |
 **z_report_number** | **int**|  |
 **lang** | **string**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **xReport**
> \Swagger\Client\Model\XReportResponse xReport($cash_register_uuid)

Generates a X-report for the cash register



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CashregisterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cash_register_uuid = "cash_register_uuid_example"; // string | 

try {
    $result = $apiInstance->xReport($cash_register_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashregisterApi->xReport: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_register_uuid** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\XReportResponse**](../Model/XReportResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

