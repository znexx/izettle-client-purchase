# Swagger\Client\PurchaseApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPurchaseDetailsLegacy**](PurchaseApi.md#getPurchaseDetailsLegacy) | **GET** /purchase/{purchaseUUID} | 
[**getReceiptOwnerEmail**](PurchaseApi.md#getReceiptOwnerEmail) | **GET** /purchase/{purchaseUUID}/receipt/owner | Get receipt owner email address
[**purchaseReceiptPrinted**](PurchaseApi.md#purchaseReceiptPrinted) | **POST** /purchase/{purchaseUUID}/receiptprinted | Notifies the cash register about the receipt printed events
[**purchaseReceiptSent**](PurchaseApi.md#purchaseReceiptSent) | **POST** /purchase/{purchaseUUID}/receiptcopyprinted | 
[**sendPurchaseReceipt**](PurchaseApi.md#sendPurchaseReceipt) | **POST** /purchase/{purchaseUUID}/receipt/send | Send purchase receipt
[**sendPurchaseReceiptCopy**](PurchaseApi.md#sendPurchaseReceiptCopy) | **POST** /purchase/{purchaseUUID}/receiptcopy/send | Send purchase receipt copy
[**viewPurchaseReceipt**](PurchaseApi.md#viewPurchaseReceipt) | **GET** /purchase/receipt/token/{token}/view | Render HTML purchase receipt
[**viewPurchaseReceipt1**](PurchaseApi.md#viewPurchaseReceipt1) | **GET** /purchase/receipt/{purchaseUUID}/view | Render HTML purchase receipt


# **getPurchaseDetailsLegacy**
> getPurchaseDetailsLegacy()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PurchaseApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->getPurchaseDetailsLegacy();
} catch (Exception $e) {
    echo 'Exception when calling PurchaseApi->getPurchaseDetailsLegacy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getReceiptOwnerEmail**
> \Swagger\Client\Model\ReceiptOwnerEmailResponse getReceiptOwnerEmail($purchase_uuid)

Get receipt owner email address



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PurchaseApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$purchase_uuid = "purchase_uuid_example"; // string | 

try {
    $result = $apiInstance->getReceiptOwnerEmail($purchase_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseApi->getReceiptOwnerEmail: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchase_uuid** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\ReceiptOwnerEmailResponse**](../Model/ReceiptOwnerEmailResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseReceiptPrinted**
> purchaseReceiptPrinted($purchase_uuid)

Notifies the cash register about the receipt printed events



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PurchaseApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$purchase_uuid = "purchase_uuid_example"; // string | 

try {
    $apiInstance->purchaseReceiptPrinted($purchase_uuid);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseApi->purchaseReceiptPrinted: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchase_uuid** | [**string**](../Model/.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseReceiptSent**
> purchaseReceiptSent($purchase_uuid)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PurchaseApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$purchase_uuid = "purchase_uuid_example"; // string | 

try {
    $apiInstance->purchaseReceiptSent($purchase_uuid);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseApi->purchaseReceiptSent: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchase_uuid** | [**string**](../Model/.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sendPurchaseReceipt**
> sendPurchaseReceipt($purchase_uuid, $email, $country_code, $phone_number)

Send purchase receipt



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PurchaseApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$purchase_uuid = "purchase_uuid_example"; // string | 
$email = "email_example"; // string | 
$country_code = "country_code_example"; // string | 
$phone_number = "phone_number_example"; // string | 

try {
    $apiInstance->sendPurchaseReceipt($purchase_uuid, $email, $country_code, $phone_number);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseApi->sendPurchaseReceipt: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchase_uuid** | [**string**](../Model/.md)|  |
 **email** | **string**|  | [optional]
 **country_code** | **string**|  | [optional]
 **phone_number** | **string**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sendPurchaseReceiptCopy**
> sendPurchaseReceiptCopy($purchase_uuid, $email, $country_code, $phone_number)

Send purchase receipt copy



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PurchaseApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$purchase_uuid = "purchase_uuid_example"; // string | 
$email = "email_example"; // string | 
$country_code = "country_code_example"; // string | 
$phone_number = "phone_number_example"; // string | 

try {
    $apiInstance->sendPurchaseReceiptCopy($purchase_uuid, $email, $country_code, $phone_number);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseApi->sendPurchaseReceiptCopy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchase_uuid** | [**string**](../Model/.md)|  |
 **email** | **string**|  | [optional]
 **country_code** | **string**|  | [optional]
 **phone_number** | **string**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **viewPurchaseReceipt**
> string viewPurchaseReceipt($token)

Render HTML purchase receipt



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PurchaseApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$token = "token_example"; // string | 

try {
    $result = $apiInstance->viewPurchaseReceipt($token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseApi->viewPurchaseReceipt: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **string**|  |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **viewPurchaseReceipt1**
> string viewPurchaseReceipt1($purchase_uuid)

Render HTML purchase receipt



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PurchaseApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$purchase_uuid = "purchase_uuid_example"; // string | 

try {
    $result = $apiInstance->viewPurchaseReceipt1($purchase_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseApi->viewPurchaseReceipt1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchase_uuid** | [**string**](../Model/.md)|  |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

