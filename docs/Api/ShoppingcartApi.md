# Swagger\Client\ShoppingcartApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create1**](ShoppingcartApi.md#create1) | **POST** /shopping-cart | Create shopping cart
[**createRefund1**](ShoppingcartApi.md#createRefund1) | **POST** /shopping-cart/refund | Create refund shopping cart


# **create1**
> \Swagger\Client\Model\CreateShoppingCartResponse create1($body)

Create shopping cart



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ShoppingcartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\CreateShoppingCartRequest(); // \Swagger\Client\Model\CreateShoppingCartRequest | 

try {
    $result = $apiInstance->create1($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ShoppingcartApi->create1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\CreateShoppingCartRequest**](../Model/CreateShoppingCartRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CreateShoppingCartResponse**](../Model/CreateShoppingCartResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createRefund1**
> \Swagger\Client\Model\CreateShoppingCartResponse createRefund1($body)

Create refund shopping cart



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ShoppingcartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\CreateRefundShoppingCartRequest(); // \Swagger\Client\Model\CreateRefundShoppingCartRequest | 

try {
    $result = $apiInstance->createRefund1($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ShoppingcartApi->createRefund1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\CreateRefundShoppingCartRequest**](../Model/CreateRefundShoppingCartRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CreateShoppingCartResponse**](../Model/CreateShoppingCartResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

