# Swagger\Client\Cashregisterv2Api

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**close1**](Cashregisterv2Api.md#close1) | **POST** /cashregister/v2/{cashRegisterUUID}/close | Closes the cash register and generates a Z-report
[**createCashRegisterPurchase**](Cashregisterv2Api.md#createCashRegisterPurchase) | **POST** /cashregister/v2/{cashRegisterUUID}/purchase | Create purchase with a specific cash register. After we save a receipt, it will contact the ccu to get a control code
[**createCashRegisterPurchaseRefund1**](Cashregisterv2Api.md#createCashRegisterPurchaseRefund1) | **POST** /cashregister/v2/{cashRegisterUUID}/purchase/refund | Create refund purchase with a specific cash register. After we save a receipt, it will contact the ccu to get a control code
[**getZReport1**](Cashregisterv2Api.md#getZReport1) | **GET** /cashregister/v2/{cashRegisterUUID}/zreport/{zReportNumber} | Get a specific Z-report
[**xReport1**](Cashregisterv2Api.md#xReport1) | **GET** /cashregister/v2/{cashRegisterUUID}/xreport | Generates a X-report for the cash register


# **close1**
> \Swagger\Client\Model\ZReportResponseV2 close1($cash_register_uuid)

Closes the cash register and generates a Z-report



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\Cashregisterv2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cash_register_uuid = "cash_register_uuid_example"; // string | 

try {
    $result = $apiInstance->close1($cash_register_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling Cashregisterv2Api->close1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_register_uuid** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\ZReportResponseV2**](../Model/ZReportResponseV2.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createCashRegisterPurchase**
> \Swagger\Client\Model\PurchaseReceiptResponse createCashRegisterPurchase($cash_register_uuid, $body)

Create purchase with a specific cash register. After we save a receipt, it will contact the ccu to get a control code



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\Cashregisterv2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cash_register_uuid = "cash_register_uuid_example"; // string | 
$body = new \Swagger\Client\Model\CreatePurchaseRequest(); // \Swagger\Client\Model\CreatePurchaseRequest | 

try {
    $result = $apiInstance->createCashRegisterPurchase($cash_register_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling Cashregisterv2Api->createCashRegisterPurchase: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_register_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\CreatePurchaseRequest**](../Model/CreatePurchaseRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PurchaseReceiptResponse**](../Model/PurchaseReceiptResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createCashRegisterPurchaseRefund1**
> \Swagger\Client\Model\PurchaseReceiptResponse createCashRegisterPurchaseRefund1($cash_register_uuid, $body)

Create refund purchase with a specific cash register. After we save a receipt, it will contact the ccu to get a control code



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\Cashregisterv2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cash_register_uuid = "cash_register_uuid_example"; // string | 
$body = new \Swagger\Client\Model\CreateRefundPurchaseRequest(); // \Swagger\Client\Model\CreateRefundPurchaseRequest | 

try {
    $result = $apiInstance->createCashRegisterPurchaseRefund1($cash_register_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling Cashregisterv2Api->createCashRegisterPurchaseRefund1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_register_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\CreateRefundPurchaseRequest**](../Model/CreateRefundPurchaseRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PurchaseReceiptResponse**](../Model/PurchaseReceiptResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getZReport1**
> \Swagger\Client\Model\ZReportResponseV2 getZReport1($cash_register_uuid, $z_report_number)

Get a specific Z-report



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\Cashregisterv2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cash_register_uuid = "cash_register_uuid_example"; // string | 
$z_report_number = 789; // int | 

try {
    $result = $apiInstance->getZReport1($cash_register_uuid, $z_report_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling Cashregisterv2Api->getZReport1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_register_uuid** | [**string**](../Model/.md)|  |
 **z_report_number** | **int**|  |

### Return type

[**\Swagger\Client\Model\ZReportResponseV2**](../Model/ZReportResponseV2.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **xReport1**
> \Swagger\Client\Model\XReportResponseV2 xReport1($cash_register_uuid)

Generates a X-report for the cash register



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\Cashregisterv2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cash_register_uuid = "cash_register_uuid_example"; // string | 

try {
    $result = $apiInstance->xReport1($cash_register_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling Cashregisterv2Api->xReport1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_register_uuid** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\XReportResponseV2**](../Model/XReportResponseV2.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

