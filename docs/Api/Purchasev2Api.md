# Swagger\Client\Purchasev2Api

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create**](Purchasev2Api.md#create) | **POST** /purchase/v2 | Create purchase
[**createRefund**](Purchasev2Api.md#createRefund) | **POST** /purchase/v2/refund | Create purchase for a refund
[**getPurchaseReceipt**](Purchasev2Api.md#getPurchaseReceipt) | **GET** /purchase/v2/{purchaseUUID}/receipt | Get purchase receipt, will include cash register information if possible
[**getPurchaseV2**](Purchasev2Api.md#getPurchaseV2) | **GET** /purchase/v2/{purchaseUUID} | Get purchase details


# **create**
> \Swagger\Client\Model\PurchaseReceiptResponse create($body)

Create purchase



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\Purchasev2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\CreatePurchaseRequest(); // \Swagger\Client\Model\CreatePurchaseRequest | 

try {
    $result = $apiInstance->create($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling Purchasev2Api->create: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\CreatePurchaseRequest**](../Model/CreatePurchaseRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PurchaseReceiptResponse**](../Model/PurchaseReceiptResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createRefund**
> \Swagger\Client\Model\PurchaseReceiptResponse createRefund($body)

Create purchase for a refund



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\Purchasev2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\CreateRefundPurchaseRequest(); // \Swagger\Client\Model\CreateRefundPurchaseRequest | 

try {
    $result = $apiInstance->createRefund($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling Purchasev2Api->createRefund: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\CreateRefundPurchaseRequest**](../Model/CreateRefundPurchaseRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PurchaseReceiptResponse**](../Model/PurchaseReceiptResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPurchaseReceipt**
> \Swagger\Client\Model\PurchaseReceiptResponse getPurchaseReceipt($purchase_uuid)

Get purchase receipt, will include cash register information if possible



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\Purchasev2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$purchase_uuid = "purchase_uuid_example"; // string | 

try {
    $result = $apiInstance->getPurchaseReceipt($purchase_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling Purchasev2Api->getPurchaseReceipt: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchase_uuid** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\PurchaseReceiptResponse**](../Model/PurchaseReceiptResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPurchaseV2**
> \Swagger\Client\Model\Purchase getPurchaseV2($purchase_uuid)

Get purchase details



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\Purchasev2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$purchase_uuid = "purchase_uuid_example"; // string | 

try {
    $result = $apiInstance->getPurchaseV2($purchase_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling Purchasev2Api->getPurchaseV2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchase_uuid** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\Purchase**](../Model/Purchase.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

