# Swagger\Client\Purchasesv2Api

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPurchaseHistory**](Purchasesv2Api.md#getPurchaseHistory) | **GET** /purchases/v2 | Get purchase history


# **getPurchaseHistory**
> \Swagger\Client\Model\PurchaseHistoryResponse getPurchaseHistory($last_purchase_hash, $start_date, $end_date, $limit, $descending)

Get purchase history



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\Purchasesv2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$last_purchase_hash = "last_purchase_hash_example"; // string | 
$start_date = "start_date_example"; // string | 
$end_date = "end_date_example"; // string | 
$limit = 56; // int | 
$descending = false; // bool | 

try {
    $result = $apiInstance->getPurchaseHistory($last_purchase_hash, $start_date, $end_date, $limit, $descending);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling Purchasesv2Api->getPurchaseHistory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **last_purchase_hash** | **string**|  | [optional]
 **start_date** | **string**|  | [optional]
 **end_date** | **string**|  | [optional]
 **limit** | **int**|  | [optional]
 **descending** | **bool**|  | [optional] [default to false]

### Return type

[**\Swagger\Client\Model\PurchaseHistoryResponse**](../Model/PurchaseHistoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

