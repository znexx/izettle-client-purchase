# CreateRefundShoppingCartRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source** | **string** |  | [optional] 
**application_id** | **string** |  | [optional] 
**application_key** | **string** |  | [optional] 
**receiver_organization_reference** | **string** |  | [optional] 
**refunds_purchase_uuid** | **string** |  | 
**gps_coordinates** | [**\Swagger\Client\Model\GpsCoordinates**](GpsCoordinates.md) |  | [optional] 
**products** | [**\Swagger\Client\Model\ItemLine[]**](ItemLine.md) |  | [optional] 
**discounts** | [**\Swagger\Client\Model\Discount[]**](Discount.md) |  | [optional] 
**service_charge** | [**\Swagger\Client\Model\ServiceCharge**](ServiceCharge.md) |  | [optional] 
**currency** | **string** |  | 
**country** | **string** |  | [optional] 
**references** | **map[string,object]** |  | [optional] 
**remote_sale_information** | [**\Swagger\Client\Model\RemoteSaleInformation**](RemoteSaleInformation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


