# CashRegister

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** |  | [optional] 
**open** | **bool** |  | [optional] 
**display_name** | **string** |  | [optional] 
**location** | **string** |  | [optional] 
**cashier_name** | **string** |  | [optional] 
**balance** | **int** |  | [optional] 
**opening_date** | **int** |  | [optional] 
**udid** | **string** |  | [optional] 
**ccu_register_id** | **string** |  | [optional] 
**initial_cash_exchange** | **int** |  | [optional] 
**active** | **bool** |  | [optional] 
**user_id** | **int** |  | [optional] 
**organization_uuid** | **string** |  | [optional] 
**active_user_uuid** | **string** |  | [optional] 
**cash_register_uuid** | **string** |  | [optional] 
**active_user_name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


