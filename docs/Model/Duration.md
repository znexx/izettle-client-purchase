# Duration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seconds** | **int** |  | [optional] 
**nano** | **int** |  | [optional] 
**units** | [**\Swagger\Client\Model\TemporalUnit[]**](TemporalUnit.md) |  | [optional] 
**negative** | **bool** |  | [optional] 
**zero** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


