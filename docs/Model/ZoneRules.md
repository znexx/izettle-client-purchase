# ZoneRules

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fixed_offset** | **bool** |  | [optional] 
**transitions** | [**\Swagger\Client\Model\ZoneOffsetTransition[]**](ZoneOffsetTransition.md) |  | [optional] 
**transition_rules** | [**\Swagger\Client\Model\ZoneOffsetTransitionRule[]**](ZoneOffsetTransitionRule.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


