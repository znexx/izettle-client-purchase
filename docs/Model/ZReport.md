# ZReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**closing_date** | **int** |  | [optional] 
**cash_register_uuid** | **string** |  | [optional] 
**sequence_nr** | **int** |  | [optional] 
**cashier_name** | **string** |  | [optional] 
**cash_register_display_name** | **string** |  | [optional] 
**opening_date** | **int** |  | [optional] 
**amount** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


