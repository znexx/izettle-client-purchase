# CreatePurchaseRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source** | **string** |  | [optional] 
**gps_coordinates** | [**\Swagger\Client\Model\GpsCoordinates**](GpsCoordinates.md) |  | [optional] 
**products** | [**\Swagger\Client\Model\ItemLine[]**](ItemLine.md) |  | [optional] 
**discounts** | [**\Swagger\Client\Model\Discount[]**](Discount.md) |  | [optional] 
**payments** | [**\Swagger\Client\Model\Payment[]**](Payment.md) |  | [optional] 
**service_charge** | [**\Swagger\Client\Model\ServiceCharge**](ServiceCharge.md) |  | [optional] 
**currency** | **string** |  | 
**country** | **string** |  | 
**references** | **map[string,object]** |  | [optional] 
**shopping_cart_uuid** | **string** |  | [optional] 
**refunds_purchase_uuid** | **string** |  | [optional] 
**uuid** | **string** |  | [optional] 
**remote_sale_information** | [**\Swagger\Client\Model\RemoteSaleInformation**](RemoteSaleInformation.md) |  | [optional] 
**refund** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


