# PurchaseHistoryResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purchases** | [**\Swagger\Client\Model\Purchase[]**](Purchase.md) |  | [optional] 
**first_purchase_hash** | **string** |  | [optional] 
**last_purchase_hash** | **string** |  | [optional] 
**link_urls** | **string[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


