# DiscountSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nr_of** | **int** |  | [optional] 
**amount** | **int** |  | [optional] 
**vat_amount** | **int** |  | [optional] 
**net_amount** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


