# CashRegisterSystemInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manufacturer** | **string** |  | [optional] 
**manufacturing_model** | **string** |  | [optional] 
**manufacturing_number** | **string** |  | [optional] 
**manufacturing_version** | **string** |  | [optional] 
**ccu_manufacturing_number** | **string** |  | [optional] 
**cash_register_model** | **string** |  | [optional] 
**support_enabled** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


