# TaxCodeSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tax_code** | **string** |  | [optional] 
**quantity** | **float** |  | [optional] 
**unit_name** | **string** |  | [optional] 
**amount** | **int** |  | [optional] 
**vat_amount** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


