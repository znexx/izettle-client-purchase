# ZoneOffsetTransitionRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**month** | **string** |  | [optional] 
**time_definition** | **string** |  | [optional] 
**standard_offset** | [**\Swagger\Client\Model\ZoneOffset**](ZoneOffset.md) |  | [optional] 
**offset_before** | [**\Swagger\Client\Model\ZoneOffset**](ZoneOffset.md) |  | [optional] 
**offset_after** | [**\Swagger\Client\Model\ZoneOffset**](ZoneOffset.md) |  | [optional] 
**day_of_week** | **string** |  | [optional] 
**day_of_month_indicator** | **int** |  | [optional] 
**local_time** | [**\Swagger\Client\Model\LocalTime**](LocalTime.md) |  | [optional] 
**midnight_end_of_day** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


