# ZReportResponseV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**merchant** | [**\Swagger\Client\Model\Merchant**](Merchant.md) |  | [optional] 
**cash_register** | [**\Swagger\Client\Model\CashRegister**](CashRegister.md) |  | [optional] 
**created** | **int** |  | [optional] 
**sequence_nr** | **int** |  | [optional] 
**purchase_amount** | **int** |  | [optional] 
**purchase_vat_amount** | **int** |  | [optional] 
**refund_amount** | **int** |  | [optional] 
**refund_vat_amount** | **int** |  | [optional] 
**nr_purchases** | **int** |  | [optional] 
**nr_refunds** | **int** |  | [optional] 
**payment_summary** | [**\Swagger\Client\Model\PaymentSummaryResponse[]**](PaymentSummaryResponse.md) |  | [optional] 
**withdrawals** | [**\Swagger\Client\Model\Withdrawal[]**](Withdrawal.md) |  | [optional] 
**line_corrections_summary** | [**\Swagger\Client\Model\LineCorrectionsSummary**](LineCorrectionsSummary.md) |  | [optional] 
**discount_summary** | [**\Swagger\Client\Model\DiscountSummary**](DiscountSummary.md) |  | [optional] 
**grouped_purchase_vat** | [**\Swagger\Client\Model\VatSummaryEntry[]**](VatSummaryEntry.md) |  | [optional] 
**grouped_refund_vat** | [**\Swagger\Client\Model\VatSummaryEntry[]**](VatSummaryEntry.md) |  | [optional] 
**grouped_vat** | [**\Swagger\Client\Model\VatSummaryEntry[]**](VatSummaryEntry.md) |  | [optional] 
**product_summary** | [**\Swagger\Client\Model\ProductSummary[]**](ProductSummary.md) |  | [optional] 
**tax_code_summaries** | [**\Swagger\Client\Model\TaxCodeSummary[]**](TaxCodeSummary.md) |  | [optional] 
**nr_receipt_copies** | **int** |  | [optional] 
**receipt_copies_amount** | **int** |  | [optional] 
**grand_total_purchase_amount** | **int** |  | [optional] 
**grand_total_refund_amount** | **int** |  | [optional] 
**nr_withdrawals** | **int** |  | [optional] 
**withdrawals_amount** | **int** |  | [optional] 
**nr_drawer_openings** | **int** |  | [optional] 
**nr_products** | **float** |  | [optional] 
**gratuity_amount** | **int** |  | [optional] 
**amount** | **int** |  | [optional] 
**vat_amount** | **int** |  | [optional] 
**net_amount** | **int** |  | [optional] 
**purchase_net_amount** | **int** |  | [optional] 
**refund_net_amount** | **int** |  | [optional] 
**grand_total_amount** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


