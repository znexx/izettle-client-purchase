# CreateRefundPurchaseRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shopping_cart_uuid** | **string** |  | 
**payments** | [**\Swagger\Client\Model\Payment[]**](Payment.md) |  | [optional] 
**gratuity_amount** | **int** |  | [optional] 
**uuid** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


