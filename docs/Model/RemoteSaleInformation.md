# RemoteSaleInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_name** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**delivery_address** | [**\Swagger\Client\Model\Address**](Address.md) |  | [optional] 
**billing_address** | [**\Swagger\Client\Model\Address**](Address.md) |  | [optional] 
**custom_fields** | [**\Swagger\Client\Model\CustomField[]**](CustomField.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


