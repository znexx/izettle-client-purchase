# GpsCoordinates

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**longitude** | **double** |  | 
**latitude** | **double** |  | 
**accuracy_meters** | **double** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


