# ZReportHistoryResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_report_hash** | **int** |  | [optional] 
**last_report_hash** | **int** |  | [optional] 
**zreports** | [**\Swagger\Client\Model\ZReport[]**](ZReport.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


