# PurchaseReceiptResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purchase_uuid** | **string** |  | [optional] 
**merchant** | [**\Swagger\Client\Model\Merchant**](Merchant.md) |  | [optional] 
**receipt_products** | [**\Swagger\Client\Model\ReceiptProduct[]**](ReceiptProduct.md) |  | [optional] 
**receipt_discounts** | [**\Swagger\Client\Model\ReceiptDiscount[]**](ReceiptDiscount.md) |  | [optional] 
**receipt_service_charge** | [**\Swagger\Client\Model\ReceiptServiceCharge**](ReceiptServiceCharge.md) |  | [optional] 
**card_payments** | [**\Swagger\Client\Model\ReceiptCardPayment[]**](ReceiptCardPayment.md) |  | [optional] 
**cash_payments** | [**\Swagger\Client\Model\ReceiptCashPayment[]**](ReceiptCashPayment.md) |  | [optional] 
**payments** | [**\Swagger\Client\Model\Payment[]**](Payment.md) |  | [optional] 
**purchase_number** | **int** |  | [optional] 
**user_display_name** | **string** |  | [optional] 
**timestamp** | **int** |  | [optional] 
**control_unit_id** | **string** |  | [optional] 
**cash_register_display_name** | **string** |  | [optional] 
**ccu_register_id** | **string** |  | [optional] 
**cash_register_uuid** | **string** |  | [optional] 
**refunds_purchase_uuid** | **string** |  | [optional] 
**global_purchase_number** | **int** |  | [optional] 
**gratuity_amount** | **int** |  | [optional] 
**refunded_by** | [**\Swagger\Client\Model\RefundInformation[]**](RefundInformation.md) |  | [optional] 
**gps_coordinates** | [**\Swagger\Client\Model\GpsCoordinates**](GpsCoordinates.md) |  | [optional] 
**source** | **string** |  | [optional] 
**remote_sale_information** | [**\Swagger\Client\Model\RemoteSaleInformation**](RemoteSaleInformation.md) |  | [optional] 
**references** | **map[string,object]** |  | [optional] 
**amount** | **int** |  | [optional] 
**refund** | **bool** |  | [optional] 
**payed_amount** | **int** |  | [optional] 
**total_amount** | **int** |  | [optional] 
**purchase_uuid1** | **string** |  | [optional] 
**rounded_amount** | **int** |  | [optional] 
**total_vat_amount** | **int** |  | [optional] 
**vat_values** | [**\Swagger\Client\Model\AggregatedVatValue[]**](AggregatedVatValue.md) |  | [optional] 
**cash_register_uuid1** | **string** |  | [optional] 
**refunds_purchase_uuid1** | **string** |  | [optional] 
**refunded** | **bool** |  | [optional] 
**fully_refunded** | **bool** |  | [optional] 
**total_fee** | **int** |  | [optional] 
**receiver_bank_statement** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


