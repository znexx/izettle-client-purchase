# CashPayment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cash_payment_uuid** | **string** |  | [optional] 
**amount** | **int** |  | 
**handed_amount** | **int** |  | 
**gratuity_amount** | **int** |  | [optional] 
**cash_payment_uuid1** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


