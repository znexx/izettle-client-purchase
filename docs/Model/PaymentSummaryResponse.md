# PaymentSummaryResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_type** | **string** |  | [optional] 
**amount** | **int** |  | [optional] 
**vat_amount** | **int** |  | [optional] 
**refund_amount** | **int** |  | [optional] 
**refund_vat_amount** | **int** |  | [optional] 
**nr_payments** | **int** |  | [optional] 
**nr_refunds** | **int** |  | [optional] 
**net_amount** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


