# LineCorrectionsSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_removed_summary** | [**\Swagger\Client\Model\AmountsSummary**](AmountsSummary.md) |  | [optional] 
**sale_cancellation_summary** | [**\Swagger\Client\Model\AmountsSummary**](AmountsSummary.md) |  | [optional] 
**product_quantity_changed_summary** | [**\Swagger\Client\Model\AmountsSummary**](AmountsSummary.md) |  | [optional] 
**discount_added_summary** | [**\Swagger\Client\Model\AmountsSummary**](AmountsSummary.md) |  | [optional] 
**discount_removed_summary** | [**\Swagger\Client\Model\AmountsSummary**](AmountsSummary.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


