# Merchant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**legal_entity_nr** | **string** |  | [optional] 
**legal_entity_type** | **string** |  | [optional] 
**country_id** | **string** |  | [optional] 
**vat_nr** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**address** | **string** |  | [optional] 
**address_line2** | **string** |  | [optional] 
**zip_code** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**phone_number** | **string** |  | [optional] 
**optional_text** | **string** |  | [optional] 
**web_site** | **string** |  | [optional] 
**contact_email** | **string** |  | [optional] 
**receipt_email** | **string** |  | [optional] 
**organization_uuid** | **string** |  | [optional] 
**language_id** | **string** |  | [optional] 
**currency_id** | **string** |  | [optional] 
**time_zone** | [**\Swagger\Client\Model\ZoneId**](ZoneId.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


