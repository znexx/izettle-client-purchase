# CashRegisterResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | **string** |  | [optional] 
**uuid** | **string** |  | [optional] 
**cashier_name** | **string** |  | [optional] 
**location** | **string** |  | [optional] 
**balance** | **int** |  | [optional] 
**cash_balance** | **int** |  | [optional] 
**opening_date** | **int** |  | [optional] 
**cash_register_system_information** | [**\Swagger\Client\Model\CashRegisterSystemInformation**](CashRegisterSystemInformation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


