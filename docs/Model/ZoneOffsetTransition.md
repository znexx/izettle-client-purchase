# ZoneOffsetTransition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset_before** | [**\Swagger\Client\Model\ZoneOffset**](ZoneOffset.md) |  | [optional] 
**offset_after** | [**\Swagger\Client\Model\ZoneOffset**](ZoneOffset.md) |  | [optional] 
**duration** | [**\Swagger\Client\Model\Duration**](Duration.md) |  | [optional] 
**gap** | **bool** |  | [optional] 
**date_time_before** | [**\DateTime**](\DateTime.md) |  | [optional] 
**date_time_after** | [**\DateTime**](\DateTime.md) |  | [optional] 
**instant** | **int** |  | [optional] 
**overlap** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


