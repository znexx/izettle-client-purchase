# CardPayment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**card_payment_uuid** | **string** |  | 
**amount** | **int** |  | 
**card_type** | **string** |  | [optional] 
**gratuity_amount** | **int** |  | [optional] 
**masked_pan** | **string** |  | [optional] 
**card_payment_entry_mode** | **string** |  | [optional] 
**application_identifier** | **string** |  | [optional] 
**application_name** | **string** |  | [optional] 
**terminal_verification_results** | **string** |  | [optional] 
**transaction_status_information** | **string** |  | [optional] 
**reference_number** | **string** |  | [optional] 
**nr_of_installments** | **int** |  | [optional] 
**installment_amount** | **int** |  | [optional] 
**card_payment_uuid1** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


