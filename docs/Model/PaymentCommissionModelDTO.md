# PaymentCommissionModelDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fixed** | **int** |  | [optional] 
**percentage** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


