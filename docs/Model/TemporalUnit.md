# TemporalUnit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_based** | **bool** |  | [optional] 
**time_based** | **bool** |  | [optional] 
**duration** | [**\Swagger\Client\Model\Duration**](Duration.md) |  | [optional] 
**duration_estimated** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


