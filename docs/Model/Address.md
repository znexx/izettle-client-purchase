# Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **string** |  | [optional] 
**country_code** | **string** |  | [optional] 
**line1** | **string** |  | [optional] 
**line2** | **string** |  | [optional] 
**zipcode** | **string** |  | [optional] 
**state_code** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


