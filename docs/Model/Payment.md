# Payment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** |  | 
**receiver_organization** | **string** |  | [optional] 
**amount** | **int** |  | 
**type** | **string** |  | 
**gratuity_amount** | **int** |  | [optional] 
**currency** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**reference_number** | **string** |  | [optional] 
**references** | **map[string,object]** |  | [optional] 
**commission** | [**\Swagger\Client\Model\PaymentCommissionDTO**](PaymentCommissionDTO.md) |  | [optional] 
**created_at** | **int** |  | [optional] 
**details** | **object** |  | [optional] 
**attributes** | **map[string,object]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


