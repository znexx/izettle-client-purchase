# CashRegistersResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cash_registers** | [**\Swagger\Client\Model\CashRegister[]**](CashRegister.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


