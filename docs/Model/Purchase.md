# Purchase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source** | **string** |  | [optional] 
**purchase_uuid** | **string** |  | 
**amount** | **int** |  | 
**vat_amount** | **int** |  | [optional] 
**country** | **string** |  | 
**currency** | **string** |  | 
**timestamp** | **int** |  | 
**gps_coordinates** | [**\Swagger\Client\Model\GpsCoordinates**](GpsCoordinates.md) |  | [optional] 
**purchase_number** | **int** |  | [optional] 
**global_purchase_number** | **int** |  | [optional] 
**user_display_name** | **string** |  | [optional] 
**user_id** | **int** |  | 
**organization_id** | **int** |  | 
**products** | [**\Swagger\Client\Model\ItemLine[]**](ItemLine.md) |  | [optional] 
**discounts** | [**\Swagger\Client\Model\Discount[]**](Discount.md) |  | [optional] 
**service_charge** | [**\Swagger\Client\Model\ServiceCharge**](ServiceCharge.md) |  | [optional] 
**card_payments** | [**\Swagger\Client\Model\CardPayment[]**](CardPayment.md) |  | [optional] 
**cash_payments** | [**\Swagger\Client\Model\CashPayment[]**](CashPayment.md) |  | [optional] 
**payments** | [**\Swagger\Client\Model\Payment[]**](Payment.md) |  | [optional] 
**refunded_by_purchase_uui_ds** | **string[]** |  | [optional] 
**refunds_purchase_uuid** | **string** |  | [optional] 
**cash_register** | [**\Swagger\Client\Model\CashRegisterDetails**](CashRegisterDetails.md) |  | [optional] 
**receipt_copy_allowed** | **bool** |  | [optional] 
**gratuity_amount** | **int** |  | [optional] 
**references** | **map[string,object]** |  | [optional] 
**remote_sale_information** | [**\Swagger\Client\Model\RemoteSaleInformation**](RemoteSaleInformation.md) |  | [optional] 
**created** | **int** |  | 
**grouped_vat_amounts** | **map[string,int]** |  | [optional] 
**purchase_uuid1** | **string** |  | [optional] 
**refunds_purchase_uuid1** | **string** |  | [optional] 
**refunded** | **bool** |  | [optional] 
**refunded_by_purchase_uui_ds1** | **string[]** |  | [optional] 
**refund** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


