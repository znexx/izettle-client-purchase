# ServiceCharge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **string** |  | [optional] 
**amount** | **int** |  | [optional] 
**percentage** | **double** |  | [optional] 
**vat_percentage** | **float** |  | [optional] 
**quantity** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


