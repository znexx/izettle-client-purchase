# ReceiptProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**quantity** | **float** |  | 
**library_product** | **bool** |  | [optional] 
**vat_percentage** | **float** |  | [optional] 
**tax_code** | **string** |  | [optional] 
**unit_price** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**variant_name** | **string** |  | [optional] 
**unit_name** | **string** |  | [optional] 
**discount** | [**\Swagger\Client\Model\ProductDiscount**](ProductDiscount.md) |  | [optional] 
**category** | [**\Swagger\Client\Model\ProductCategory**](ProductCategory.md) |  | [optional] 
**comment** | **string** |  | [optional] 
**gross_value** | **int** |  | [optional] 
**discount_value** | **int** |  | [optional] 
**type** | **string** |  | [optional] 
**details** | **object** |  | [optional] 
**total_amount** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


