# ItemLine

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**quantity** | **float** |  | 
**product_uuid** | **string** |  | [optional] 
**variant_uuid** | **string** |  | [optional] 
**vat_percentage** | **float** |  | [optional] 
**tax_code** | **string** |  | [optional] 
**unit_price** | **int** |  | 
**cost_price** | **int** |  | [optional] 
**row_taxable_amount** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**variant_name** | **string** |  | [optional] 
**sku** | **string** |  | [optional] 
**barcode** | **string** |  | [optional] 
**image_lookup_key** | **string** |  | [optional] 
**unit_name** | **string** |  | [optional] 
**discount** | [**\Swagger\Client\Model\ProductDiscount**](ProductDiscount.md) |  | [optional] 
**category** | [**\Swagger\Client\Model\ProductCategory**](ProductCategory.md) |  | [optional] 
**discount_value** | **int** |  | [optional] 
**comment** | **string** |  | [optional] 
**from_location_uuid** | **string** |  | [optional] 
**to_location_uuid** | **string** |  | [optional] 
**auto_generated** | **bool** |  | [optional] 
**id** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**details** | **object** |  | [optional] 
**library_product** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


