# PaymentCommissionDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_amount** | **int** |  | [optional] 
**vat_amount** | **int** |  | [optional] 
**vat_rate** | **float** |  | [optional] 
**model_id** | **string** |  | [optional] 
**model** | [**\Swagger\Client\Model\PaymentCommissionModelDTO**](PaymentCommissionModelDTO.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


