# ReceiptDiscount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**amount** | **int** |  | [optional] 
**percentage** | **double** |  | [optional] 
**quantity** | **float** |  | [optional] 
**total_amount** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


