# ZoneOffset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_seconds** | **int** |  | [optional] 
**id** | **string** |  | [optional] 
**rules** | [**\Swagger\Client\Model\ZoneRules**](ZoneRules.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


