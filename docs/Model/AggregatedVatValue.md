# AggregatedVatValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vat_value** | **double** |  | [optional] 
**vat_amount** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


