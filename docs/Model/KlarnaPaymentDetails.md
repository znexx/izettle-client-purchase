# KlarnaPaymentDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**klarna_order_id** | **string** |  | [optional] 
**klarna_product** | **string** |  | [optional] 
**auto_capture** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


