# VatSummaryEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vat_percentage** | **double** |  | [optional] 
**full_amount** | **int** |  | [optional] 
**amount** | **int** |  | [optional] 
**taxable_amount** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


