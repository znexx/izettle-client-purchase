# AmountsSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_type** | **string** |  | [optional] 
**count** | **int** |  | [optional] 
**total_amount** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


