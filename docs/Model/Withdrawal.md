# Withdrawal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cash_register_uuid** | **string** |  | 
**created** | **int** |  | 
**user_uuid** | **string** |  | 
**user_display_name** | **string** |  | 
**amount** | **int** |  | 
**note** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


