<?php
/**
 * WithdrawalTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.12
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * WithdrawalTest Class Doc Comment
 *
 * @category    Class
 * @description Withdrawal
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class WithdrawalTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Withdrawal"
     */
    public function testWithdrawal()
    {
    }

    /**
     * Test attribute "cash_register_uuid"
     */
    public function testPropertyCashRegisterUuid()
    {
    }

    /**
     * Test attribute "created"
     */
    public function testPropertyCreated()
    {
    }

    /**
     * Test attribute "user_uuid"
     */
    public function testPropertyUserUuid()
    {
    }

    /**
     * Test attribute "user_display_name"
     */
    public function testPropertyUserDisplayName()
    {
    }

    /**
     * Test attribute "amount"
     */
    public function testPropertyAmount()
    {
    }

    /**
     * Test attribute "note"
     */
    public function testPropertyNote()
    {
    }
}
